# This script dumbly migrate from Apache's configuration 2.2 to 2.4.
#
# Here are it's current quirk:
# - it will modify config even if it's written to be cross compatible between thoses version
# - it assume "Allow from" only specifies IPs

# Order
/Order[[:blank:]]*deny,allow/Id

# * Deny
# s/Deny[[:blank:]]*from[[:blank:]]*all/Require all denied/I

/Deny[[:blank:]]*from[[:blank:]]*all/I {
    N
    s/.?\([[:blank:]]*\)Allow[[:blank:]]*from/\1Require ip/I
    s/Deny[[:blank:]]*from[[:blank:]]*all/Require all denied/I
}

s/Deny from env=/Require not env /I

# * Allow
s/Allow[[:blank:]]*from[[:blank:]]*all/Require all granted/I
s/Allow[[:blank:]]*from/Require ip/I

# * Satisfy

# It's the default value
/Satisfy[[:blank:]]*any/Id
