#!/usr/bin/env perl
#
# Replace apache's mod_access_compat directives to mod_authz_host ones, to
# securly migrate to apache 2.2 to 2.4. This script only migrate most
# common pattern.

# use re "debug";
use strict;
use warnings;

# TODO Maybe use a redo in of clauses to avoid missing substitutions

# our $^I = '.bak';

# our @ARGV = ($ARGV[0]); # We don't want to use STDIN when eof get called

# open(my $fh, "+<", $ARGV[0])
#     or die "Can't open ARGv[0]!";

# Regex for spaces bettwen word (including comment character)
my $s = '[#\s]*';

while (<>) {
    # ** Order
    # Default of mod_access_compat
    next if /Order${s}deny,allow/i;

    if (/(Order${s}Allow,Deny)/i) {
        $_ .= <> unless eof;
        # NOTE We replace with dpreceated directive, because they are
        # replaced with the current one down the line.
        s/${1}${s}(Deny${s}from${s}all)/$1/i;
        s/${1}${s}(Allow${s}from${s}localhost.*)/$1/i;
        # When someone was mixing directives
        s/${1}${s}(Require*)/$1/i;
    };

    # ** Satisfy
    # Correspond to the new default, from mod_authz_host, to have an implicit <RequireAny>
    next if /Satisfy${s}any/i;

    # ** Misc
    if (/(Require${s}valid-user)/i) {
        $_ .= <>.<>.<> unless eof;
        s/(${1})${s}Order${s}Deny,Allow${s}Deny${s}from${s}all/$1/i;
    };

    # ** Deny
    if (/(Deny${s}from${s}all)/i) {
        $_ .= <> unless eof;
        s/${1}${s}Allow${s}from/Require ip/i;
        s/Deny${s}from${s}all/Require all denied/i;
    };

    # https://bz.apache.org/bugzilla/show_bug.cgi?id=60946
    s/Deny${s}from${s}env=(!?)(\S+)/ "Require expr \"-".( $1 ? "n" : "z")." %{reqenv:$2}\""/ei;

    # ** Allow
    s/Allow${s}from${s}all/Require all granted/i;
    s/Allow${s}from${s}localhost.*/Require ip local/i;
    s/Allow${s}from/Require ip/i;

    print;
}

# close $fh;
