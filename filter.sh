#!/bin/sh

# Types: V S H T
type=$1
result=/tmp/apache-require/result

awk --assign type="$type" '{ if ($NF == type) print $0; }' "$result"
