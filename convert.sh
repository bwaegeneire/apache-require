#!/bin/sh

newline="\n[[:blank:]]"

# 2.2 Directives
all_denied="s/Order deny,allow${newline}Deny from all/Require all denied/"
## Last
deny_from_env="s/Deny from env=/Require not env /"
allow_all="s/Allow from all/Require all granted/"

# Mixed directives
mix_allow_all_require_all="s/Allow from all${newline}Require all granted/Require all granted/"
