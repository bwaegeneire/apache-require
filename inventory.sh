#!/bin/sh

# TODO Use \0 as a seprator
# TODO replace realpath as it isn't POSIX
# TODO Categorize mixed directives, thoses need to be manualy modified
# We may need a different file that list files with non mixed directives

set -e

apache_dir=/etc/apache2
apache_conf=$apache_dir/apache2.conf

tmp_dir=/tmp/apache-require
mkdir -p "$tmp_dir"
confs=$tmp_dir/confs
confs_vhost=$tmp_dir/confs_vhost
confs_system=$tmp_dir/confs_system
confs_htaccess=$tmp_dir/confs_htaccess
confs_template=$tmp_dir/confs_template
result=$tmp_dir/inventory
summary=$tmp_dir/summary

module_loaded() {
    apache2ctl -D DUMP_MODULES | grep -q access_compat_module
}

# Get all apache config files
get_confs() {
    # Initialize le the list of configuration files with the default conf
    test ! -e "$confs" && printf "%s\\n" $apache_conf > "$confs"

    cd "$apache_dir"
    # TODO: Refactor this mess
    confs_size=0
    while [ "$confs_size" -lt "$(stat -c %s "$confs")" ]; do
        confs_size=$(stat -c %s "$confs")
        for conf_file in $(cat "$confs"); do
            # Get all Include directives
            for included in $(awk '/^[[:space:]]*Include/ {print $2}' "$conf_file"); do
                # In case a directory is included even tho it's not recomended
                if [ -d "$included" ]; then
                    included=$included/*
                fi
                # XXX: Expand the filenames
                realpath --canonicalize-existing --quiet $included \
                    | grep -v mods-available >> "$confs" || true
            done
        done
        sort "$confs" | uniq > "$confs"_tmp && mv "$confs"_tmp "$confs"
    done
    cd - 1>/dev/null
}

# Find all the .htaccess under each DocumentRoot
get_htaccess() {
    xargs \
        awk 'sub("^[[:space:]]*DocumentRoot[[:space:]]*", "")' \
        < "$confs" \
        | sort -u \
        | xargs -I _ find _ -type f -name .htaccess || true
}

# Find directives in non apache location. It can be configuration for
# phpmyadmin or the default vhost template for example.
get_template() {
    find /etc /home/evoadmin \( -path /etc/apache2 -o -path /etc/squid3 \) -prune -false \
        -o -type f -exec grep -IlE \
        '^[[:space:]]*(Order|Allow|Deny|Satisfy)[[:space:]]' {} \;
}

filter_backup() {
    grep -v 'apreq.bak' || true
}

# Put different type of configs in different files
categorize_confs() {
    get_template | filter_backup > "$confs_template"
    get_htaccess | filter_backup > "$confs_htaccess"
    grep -E "^${apache_dir}/sites-available/.*\\.conf" "$confs" \
        | filter_backup > "$confs_vhost"
    grep -Ev "^${apache_dir}/sites-available/.*\\.conf" "$confs" \
        | filter_backup> "$confs_system"
}

# Count directives and return files only containing some directives
# Takes argument: file type
count_directives() {
    directives="Allow Order Deny Satisfy"

    for directive in $directives; do
        export "$directive"="$(grep -Ec "^[[:blank:]]*$directive\\s" "$1")"
    done
    # FIXME: There are probably a lot of flase negative
    export CGI="$(grep -Ec "^[[:blank:]]*# CGI" "$1")"

    # shellcheck disable=SC2154
    if [ "$Allow" -ne 0 ] || [ "$Order" -ne 0 ] || \
        [ "$Deny" -ne 0 ] || [ "$Satisfy" -ne 0 ]; then
        export Total=$(( $Allow + $Order + $Deny + $Satisfy ))
        printf "%s %d %d %d %d %d %d %s\\n" "$1" \
            "$Allow" "$Order" "$Deny" "$Satisfy" "$Total" "$CGI" "$2"
    fi
}

# Results per files
display_results() {
    mv -b "$result" "$result".bak || true

    # Types: Vhost, System, Htaccess, Template
    # printf 'File\tAllow\tOrder\tDeny\tSatisfy\tTotal (directives)\tCGI\tType (V|S|H|T)\n' >&2

    set -- \
        "$confs_vhost"      V \
        "$confs_system"     S \
        "$confs_htaccess"   H \
        "$confs_template"   T
    # For each types of confs
    while [ "$#" -gt 0 ]; do
        # For each confs file of that type
        while IFS= read -r file; do
            count_directives "$file" "$2" >> "$result"
        done < "$1"
        shift 2
    done
}

# One line machine summary of the results
display_summary() {
    mv -b "$summary" "$summary".bak || true

    # Types: Vhost, System, Htaccess, Template
    # printf 'Hostname:\tVhost\tSystem\tHtaccess\tTemplate\tTotal\tCGI\n' >&2

    printf "%s " "$(hostname)"; awk -f - "$result" <<EOF > "$summary"
BEGIN {
  total["V"] = 0;
  total["S"] = 0;
  total["H"] = 0;
  total["T"] = 0;
  cgi = 0;
}

{
    total[\$NF] += \$(NF-2);
    cgi += \$(NF-1);
}

END {
  print total["V"] " " \
        total["S"] " " \
        total["H"] " " \
        total["T"] " " \
        total["V"] + total["S"] + total["H"] + total["T"] " " \
        cgi;
}
EOF

}

get_confs
categorize_confs
display_results
display_summary
