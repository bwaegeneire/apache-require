#!/bin/sh

set -e

inventory=$1

tmp_dir=/tmp/apache-require
backup_dir=$tmp_dir/backup
inventory=$tmp_dir/inventory
to_convert=$tmp_dir/to_convert

# Create a backup of the FILE specific for this project
backup() {
    file=$1

    install --preserve-timestamps --preserve-contex \
        -D "$file" "$backup_dir/$file"
}

verify_migration() {
    if apache2ctl -t; then
        echo "the migration produced valid config"
    else
        echo "the migration went wrong, try doing it manually"
        exit 1
    fi
}

try_disabling_access_compat() {
    if a2dismod access_compat && apache2ctl -t; then
        echo "access_compat disabled succesfully!"
    else
        echo "access_compat can't be disabled, \
            some config is still using it"
        a2enmod access_compat
    fi
}

awk '{ print $1 }' "$inventory" > "$to_convert"

while IFS= read -r file; do
    echo $file
    backup "$file"
    perl -i ~bwaegeneire/convert.pl "$file"
done < "$to_convert"

verify_migration && try_disabling_access_compat
